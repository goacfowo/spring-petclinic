FROM openjdk:17-alpine

EXPOSE 8080

VOLUME /tmp

ADD /target/*.jar /*.jar

ENTRYPOINT ["java", "-jar", "/*.jar"]
